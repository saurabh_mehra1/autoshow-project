﻿using AutoShow.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoShow.Data
{
    public class VehicleDbContext:IdentityDbContext<ApplicationUser>
    {
        public VehicleDbContext(DbContextOptions<VehicleDbContext> options):base(options)
        {

        }

        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
    }
}
