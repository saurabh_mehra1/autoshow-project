﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoShow.Data;
using AutoShow.Models.Implementations;
using Microsoft.AspNetCore.Mvc;

namespace AutoShow.Controllers
{
    public class UserController : Controller
    {
        private UserOperation userOperation;

        public UserController(VehicleDbContext vehicleDbContext)
        {
            userOperation = new UserOperation(vehicleDbContext);
        }

        public async Task<IActionResult> Index()
        {
            return View(await userOperation.Read());
        }

        public async Task<IActionResult> Detail(string id)
        {
            if (id == "")
                return NotFound();

            return View(await userOperation.Detail(id));
        }
    }
}
