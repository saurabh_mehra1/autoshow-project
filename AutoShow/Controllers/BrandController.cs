﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoShow.Data;
using AutoShow.Models;
using AutoShow.Models.Implementations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AutoShow.Controllers
{
    [Authorize(Roles ="Admin")]
    public class BrandController : Controller
    {
        private BrandOperation brandOperation;

        public BrandController(VehicleDbContext vehicleDbContext)
        {
            brandOperation = new BrandOperation(vehicleDbContext);
        }

        public async Task<IActionResult> Index()
        {
            return View(await brandOperation.Read());
        }

        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Add([Bind("Name")]Brand brand)
        {
            if(ModelState.IsValid)
            {
                bool check=await brandOperation.Create(brand);
                if (check)
                    return View(nameof(Index));
            }
            return View();
        }

    }
}
