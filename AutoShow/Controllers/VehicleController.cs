﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoShow.Data;
using AutoShow.Models;
using AutoShow.Models.Implementations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace AutoShow.Controllers
{
    [Authorize(Roles ="Admin,User")]
    public class VehicleController : Controller
    {
        private VehicleOperation vehicleOperation;
        private readonly UserManager<ApplicationUser> userManager;

        public VehicleController(VehicleDbContext vehicleDbContext,UserManager<ApplicationUser> userManager)
        {
            vehicleOperation = new VehicleOperation(vehicleDbContext);
            this.userManager = userManager;
        }

        [AllowAnonymous]
        public async Task<IActionResult> Index()
        {
            return View(await vehicleOperation.Read());
        }

        [AllowAnonymous]
        public async Task<IActionResult> Detail(int? id)
        {
            if (id == null)
                return NotFound();

            var vehicle =await vehicleOperation.Detail(id.ToString());

            if (vehicle == null)
                return NotFound();

            return View(vehicle);
        }

        public IActionResult Add()
        {
            ViewData["Brands"] = new SelectList(vehicleOperation.GetBrand(), "Id", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Add([Bind("Name,Type,Price,Colour,BrandId")] Vehicle vehicle)
        {
            var asd = userManager.GetUserId(HttpContext.User);
            if (asd == null)
                return NotFound();
            vehicle.ApplicationUserId = asd;
            if (ModelState.IsValid)
            {
                bool check = false;
                check = await vehicleOperation.Create(vehicle);
                if (check)
                    return RedirectToAction(nameof(Index));
            }
            ViewData["Brands"] = new SelectList(vehicleOperation.GetBrand(), "Id", "Name", vehicle.BrandId);
            return View();
        }

        public IActionResult Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var vehicle = vehicleOperation.Search(id.ToString());

            if (vehicle == null)
                return NotFound();

            ViewData["Brands"] = new SelectList(vehicleOperation.GetBrand(), "Id", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int? id,[Bind("Name,Type,Price,Colour,BrandId")]Vehicle vehicle)
        {
            if (vehicle.Id != id)
                return NotFound();

            if(ModelState.IsValid)
            {
                bool check=await vehicleOperation.Edit(vehicle);
                if (check)
                    return RedirectToAction(nameof(Index));
            }
            ViewData["Brands"] = new SelectList(vehicleOperation.GetBrand(), "Id", "Name");
            return View();
        }

        public async  Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            var vehicle =await vehicleOperation.Detail(id.ToString());

            if (vehicle == null)
                return NotFound();

            return View(vehicle);
        }

        [HttpPost,ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async  Task<IActionResult> DeleteConfirm(int? id)
        {
            bool check =await vehicleOperation.Delete(id.ToString());
            if (check)
                return RedirectToAction(nameof(Index));
            return View();
        }

    }
}