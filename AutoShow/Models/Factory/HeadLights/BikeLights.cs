﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoShow.Models.Factory.HeadLights
{
    public class BikeLights : HeadLight
    {
        public override string HeadLightsName()
        {
            return " Bike HeadLights ";
        }
    }
}
