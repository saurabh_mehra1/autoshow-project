﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoShow.Models.Factory.HeadLights
{
    public class CarLights : HeadLight
    {
        public override string HeadLightsName()
        {
            return " Car HeadLights ";
        }
    }
}
