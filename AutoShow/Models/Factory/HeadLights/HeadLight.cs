﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoShow.Models.Factory.HeadLights
{
    public abstract class HeadLight
    {
        public abstract string HeadLightsName();
    }
}
