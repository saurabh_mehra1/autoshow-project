﻿using AutoShow.Models.Factory.HeadLights;
using AutoShow.Models.Factory.Tires;
using AutoShow.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoShow.Models.Factory
{
    public abstract class VehicleFactory
    {
        public abstract HeadLight GetHeadLight();
        public abstract Tire GetTire();
    }
}
