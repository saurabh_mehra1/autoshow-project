﻿using AutoShow.Models.Factory.HeadLights;
using AutoShow.Models.Factory.Tires;
using AutoShow.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;

namespace AutoShow.Models.Factory
{
    public class CarFactory : VehicleFactory
    {
        public override HeadLight GetHeadLight()
        {
            return new CarLights();
        }

        public override Tire GetTire()
        {
            return new CarTires();
        }
    }
}
