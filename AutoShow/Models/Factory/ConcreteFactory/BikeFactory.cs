﻿using AutoShow.Models.Factory.HeadLights;
using AutoShow.Models.Factory.Tires;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoShow.Models.Factory
{
    public class BikeFactory : VehicleFactory
    {
        public override HeadLight GetHeadLight()
        {
              return new BikeLights();
        }

        public override Tire GetTire()
        {
              return new BikeTires();
        }
    }
}
