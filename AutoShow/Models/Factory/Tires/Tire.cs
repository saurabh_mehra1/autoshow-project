﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoShow.Models.Factory.Tires
{
    public abstract class Tire
    {
        public abstract string TireName();
    }
}
