﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoShow.Models.Factory.Tires
{
    public class BikeTires : Tire
    {
        public override string TireName()
        {
            return " Bike Tires ";
        }
    }
}
