﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoShow.Models.Factory.Tires
{
    public class CarTires : Tire
    {
        public override string TireName()
        {
            return " Car Tires ";
        }
    }
}
