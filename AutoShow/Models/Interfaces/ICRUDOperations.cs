﻿using AutoShow.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoShow.Models.Interfaces
{
    public interface ICRUDOperations<T>
    {
        Task<bool> Create(T model);
        Task<List<T>> Read();
        Task<bool> Edit(T model);
        Task<bool> Delete(string id);
        Task<T> Search(string id);
        Task<T> Detail(string id);
    }
}

