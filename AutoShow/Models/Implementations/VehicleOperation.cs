﻿using AutoShow.Data;
using AutoShow.Models.Interfaces;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoShow.Models.Implementations
{
    public class VehicleOperation : ICRUDOperations<Vehicle>
    {
        private readonly VehicleDbContext vehicleDbContext;

        public VehicleOperation(VehicleDbContext vehicleDbContext)
        {
            this.vehicleDbContext = vehicleDbContext;
        }

        public async Task<bool> Create(Vehicle model)
        {
            vehicleDbContext.Vehicles.Add(model);
            int a = await vehicleDbContext.SaveChangesAsync();
            /*
            var VechId = model.Id;
            string wwrootPath = hostingEnvironment.
                */
            return a == 1;
        }

        public async Task<bool> Delete(string id)
        {
            var vehicle = await Search(id);

            if (vehicle == null)
                return false;

            vehicleDbContext.Vehicles.Remove(vehicle);
            int a = await vehicleDbContext.SaveChangesAsync();
            return a == 1;
        }

        public async Task<List<Vehicle>> Read()
        {
            var vehicles = vehicleDbContext.Vehicles.Include(m => m.Brand).Include(m => m.ApplicationUser);
            return await vehicles.ToListAsync();
        }

        public async Task<Vehicle> Search(string id)
        {
            var vehicle =await vehicleDbContext.Vehicles.FindAsync(int.Parse(id));
            return vehicle;
        }

        public async Task<bool> Edit(Vehicle model)
        {
            var vehicle = await Search(model.Id.ToString());
            if (vehicle == null)
                return false;
            vehicleDbContext.Vehicles.Update(model);

            int a = await vehicleDbContext.SaveChangesAsync();
            return a == 1;
        }

        public async Task<Vehicle> Detail(string id)
        {
            var vehicle = await vehicleDbContext.Vehicles.Include(m => m.Brand).Include(m => m.ApplicationUser).SingleOrDefaultAsync(d => d.Id == int.Parse(id));
            return vehicle;
        }

        public async Task<List<Vehicle>> MyVehicle(string id)
        {
            return await vehicleDbContext.Vehicles.Include(d=>d.Brand).Where(m => m.ApplicationUserId.Equals(id)).ToListAsync();
        }

        public DbSet<Brand> GetBrand()
        {
            return vehicleDbContext.Brands;
        }
    }
}
