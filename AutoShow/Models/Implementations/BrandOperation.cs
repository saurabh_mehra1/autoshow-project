﻿using AutoShow.Data;
using AutoShow.Models.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoShow.Models.Implementations
{
    public class BrandOperation : ICRUDOperations<Brand>
    {
        private readonly VehicleDbContext vehicleDbContext;

        public BrandOperation(VehicleDbContext vehicleDbContext)
        {
            this.vehicleDbContext = vehicleDbContext;
        }

        public async Task<bool> Create(Brand model)
        {
            vehicleDbContext.Brands.Add(model);
            int a=await vehicleDbContext.SaveChangesAsync();
            return a == 1;
        }

        public async Task<bool> Delete(string id)
        {
            Brand brand =await Search(id);

            if (brand == null)
                return false;

            vehicleDbContext.Brands.Remove(brand);
            int a = await vehicleDbContext.SaveChangesAsync();
            return a == 1;
        }

        public async Task<List<Brand>> Read()
        {
            var brands = await vehicleDbContext.Brands.ToListAsync();
            return brands;
        }

        public async Task<Brand> Search(string id)
        {
            var brand = await vehicleDbContext.Brands.FindAsync(int.Parse(id));
            return brand;
        }

        public async Task<bool> Edit(Brand model)
        {
            var obj = await Search(model.Id.ToString());

            if (obj == null)
                return false;

            vehicleDbContext.Brands.Update(model);

            int a =await vehicleDbContext.SaveChangesAsync();
            return a == 1;
        }

        public async Task<Brand> Detail(string id)
        {
            var brand = await vehicleDbContext.Brands.SingleOrDefaultAsync(d => d.Id == int.Parse(id));
            return brand;
        }

    }
}
