﻿using AutoShow.Data;
using Microsoft.AspNetCore.Identity;
using AutoShow.Models.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace AutoShow.Models.Implementations
{
    [Authorize(Roles ="Admin")]
    public class UserOperation : ICRUDOperations<ApplicationUser>
    {
        private readonly VehicleDbContext vehicleDbContext;

        public UserOperation(VehicleDbContext vehicleDbContext)
        {
            this.vehicleDbContext = vehicleDbContext;
        }

        public Task<bool> Create(ApplicationUser model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(string id)
        {
            throw new NotImplementedException();
        }

        public async Task<ApplicationUser> Detail(string id)
        {
            var user = await vehicleDbContext.ApplicationUsers.FirstOrDefaultAsync(m => m.Id == id);
            return user;
        }

        public Task<bool> Edit(ApplicationUser model)
        {
            throw new NotImplementedException();
        }

        public async Task<List<ApplicationUser>> Read()
        {
            return await vehicleDbContext.ApplicationUsers.ToListAsync();
        }

        public async Task<ApplicationUser> Search(string id)
        {
            return await vehicleDbContext.ApplicationUsers.FindAsync(id);
        }

    }
}
