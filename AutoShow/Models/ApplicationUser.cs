﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AutoShow.Models
{
    public class ApplicationUser:IdentityUser
    {
        [PersonalData]
        [Required]
        [StringLength(255)]
        public string FirstName { get; set; }
        [PersonalData]
        [Required]
        [StringLength(255)]
        public string LastName { get; set; }
        [PersonalData]
        [Required]
        [DataType("varchar(11)")]
        public string Mobile { get; set; }
        [PersonalData]
        [Required]
        [StringLength(255)]
        public string City { get; set; }
        [PersonalData]
        [Required]
        [StringLength(255)]
        public string State { get; set; }
        [PersonalData]
        [Required]
        [StringLength(255)]
        public string Country { get; set; }
    }
}
