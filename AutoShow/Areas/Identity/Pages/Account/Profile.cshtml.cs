using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using AutoShow.Data;
using AutoShow.Models;
using AutoShow.Models.Implementations;
using AutoShow.Models.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace AutoShow.Areas.Identity.Pages.Account
{
    public class ProfileModel : PageModel
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly VehicleDbContext vehicleDbContext;
        private VehicleOperation vehicleOperation;

        public ProfileModel(UserManager<ApplicationUser> userManager,VehicleDbContext vehicleDbContext)
        {
            this.userManager = userManager;
            this.vehicleDbContext = vehicleDbContext;
            this.vehicleOperation = new VehicleOperation(vehicleDbContext);
        }

        [BindProperty]
        public  InputModel Input { get; set; }
        [BindProperty]
        public List<Vehicle> Vehicle { get; set; } 

        public class InputModel
        {
            public string UserName { get; set; }
            public string Name { get; set; }
            public string Mobile { get; set; }
            public string Email { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Country { get; set; }
        }
        private async Task LoadAsync(string id)
        {
            Vehicle =await vehicleOperation.MyVehicle(id);
        }

        public async Task<IActionResult> OnGet()
        {
            var user = await userManager.GetUserAsync(User);
            Input = new InputModel {
                UserName = user.UserName,
                Name = string.Concat(user.FirstName, " ", user.LastName),
                Mobile = user.Mobile,
                Email = user.Email,
                City = user.City,
                State = user.State,
                Country = user.Country
            };
            await LoadAsync(user.Id);
            return Page();
        }
    }
}
